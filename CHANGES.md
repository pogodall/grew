## 0.46.2 (2017/12/17)
  * Fix doc and dev call to gui

## 0.46.1 (2017/12/14)
  * remove all code refering to Dep2pict

# 0.46.0 (2017/12/14)
  * Remove GUI (now available in a different opam package `grew_gui`)
  * New command line parsing of arguments, see `grew help`

## 0.45.1 (2017/12/07)
Bugfix: [#3](https://gitlab.inria.fr/grew/grew/issues/3) (Bug in GUI with graphviz 2.40)

# 0.45.0 (2017/10/10)
The version number is aligned of the version number of `libcaml-grew` (hence, the jump from 33 to 45!)
  * `-old_grs` option
  * `-fullscreen` option
  * GUI can now load a corpus: the option `-gr` has disapear, `-i` is used instead

# 0.33.0 (2017/09/05)
  * adapt to new GRS definition and restrict usage to deterministic strategies fir a given GRS.

# 0.32.0 (2017/04/18)
  * adapt to libcaml-grew 0.42.0 (dependence on yojson)
